import Customer from "./class/customer"
import Product from "./class/product"
import { Dimensions } from "./Types/dimensions"
import { Address } from "./Types/address"
import Order from "./class/order"
import Clothing from "./class/Clothing"
import Shoes from "./class/Shoes"
import { ClothingSize } from "./Types/ClothingSize"
import { ShoeSize } from "./Types/shoesize"

// Définition d'une nouvelle adresse
const newAddress: Address = {city: "Montpellier", street: "15 rue Thor", postalCode: "34000", country: "France"} 
// Création d'une instance de la classe Customer avec la nouvelle adresse crée au dessus
const customerOne = new Customer (52, "Cloé", "cloclo@gmail.com", newAddress)
console.log(customerOne)

// Définition des dimensions d'un produit
const newDimension: Dimensions =  {lenght: 54, width: 36, heigt: 69}
// Création d'un produit avec les dimensions spécifiées
const firstProduct = new Product (198, "Blue Jean", 0.200, 50, newDimension)
console.log(firstProduct)


// Création d'un vêtement avec la taille spécifiée
const clothing1 = new Clothing (198, "Blue Jean", 0.200, 50, newDimension, ClothingSize.XS)
console.log(clothing1)

// Création d'une paire de chaussures
const Shoes1 = new Shoes (198, "Blue Jean", 0.200, 50, newDimension, ShoeSize.size_38)
console.log(Shoes1)

// Création d'un tableau de produits avec les objets déja crées en amont
const products = [Shoes1, clothing1]
// Création d'une nouvelle commande avec le client, les produits et la date 
const order1 = new Order (52, customerOne, products , new Date())
console.log(order1.displayOrder())

