
import Customer from "./customer";
import Product from "./product";

// Création de la classe Order
export default class Order { 
    //Définition des propriétés de la classe
    orderId: number;
    customer: Customer;
    productsList: Product[];
    orderDate: Date; 
    // Definition du constructeur avec l'ensemble des propriétés
    constructor(orderId: number, customer: Customer, productsList: Product[], orderDate: Date) { 
        this.orderId = orderId
        this.customer = customer
        this.productsList = productsList
        this.orderDate = orderDate
    }

    //Méthode pour ajouter un produit
    addProduct(product: Product) { 
       this.productsList.push(product);
    }
    //Méthode pour supprimer un produit par son ID 
    removeProduct(productId: number) { 
        this.productsList = this.productsList.filter((product) => product.productId !== productId)
    } 

    //Méthode pour calculer le poids des produits
    calculateWeight(): number {
        let totalWeight = 0;
        for (const product of this.productsList) {
            totalWeight += product.weight;
        }
        return totalWeight;
        }
     //Méthode pour calculer le prix des produits   
    calculateTotal(): number { 
        let totalPrice = 0 
        for (const product of this.productsList) { 
            totalPrice += product.price;
        }
        return totalPrice; 
    }
    // Méthode pour afficher toutes les informations de la classe Order
    displayOrder(): string { 
        return `Order ID:" ${this.orderId}, "Customer" ${this.customer.displayInfo()}, "Products" ${this.productsList},
        "Order date:" ${this.orderDate}`
    }
}

