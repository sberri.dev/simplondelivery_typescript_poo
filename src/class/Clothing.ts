import Product from "./product";
import { ClothingSize } from "../Types/ClothingSize";
import { Dimensions } from "../Types/dimensions";

//Création de la classe enfant Clothing avec la proprietés de taille
class Clothing extends Product { 
    public size: ClothingSize
   
    // Constructeur prenant les proprietés de la classe parente  et la classe enfant 
    constructor(productId: number, name: string, weight: number, price: number, dimensions: Dimensions, size: ClothingSize) {
        super(productId, name, weight, price, dimensions);
        this.size = size;
    }
    // Méthode pour afficher les informations de la classe et de la sous classe
    displayDetails(): string { 
        return super.displayDetails() + `"Size clothing:" ${this.size}`
    }
}

export default Clothing 