import { Address } from "../Types/address";

//Création de la Classe Customer
export default class Customer { 
    // Propriétés publiques représentant les caractéristiques d'un customer
    public customerId: number;
    public name: string;
    public email: string;
    public address?: Address;

     // Constructeur de la classe Customer, initialise les propriétés du produit avec les valeurs passées en paramètre
     constructor(customerId: number, name: string, email: string, address: Address) { 
         this.customerId = customerId
         this.name = name
         this.email = email
         this.address = address
     }
     
     // Méthode pour afficher les details de la classe Customer
     displayInfo(): string { 
         return `Customer ID:" ${this.customerId}, "Name:" ${this.name}, Email: ${this.email}, 
         "Address:" ${this.address}`
     }
     
     // Méthode pour ajouter une nouvelle adresse 
     setAddress(address: Address) { 
         this.address = address
         return `"Address:" ${this.address.street} ": 15 rue Thor", ${this.address.city} ": Montpellier" 
         ${this.address.postalCode} ": 34000", ${this.address.country} ": France"`
     }
     
     // Méthode pour afficher l'adresse si elle est renseignée
     displayAddress(): string { 
        if (this.address) {
            return `"Address:" ${this.address?.street}, ${this.address?.city}, ${this.address?.postalCode}, ${this.address?.country}`
     } return "no address found"
     }
 }