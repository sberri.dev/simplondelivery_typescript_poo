import { Dimensions } from "../Types/dimensions";

//Création de la classe Product
export default class Product {
     // Propriétés publiques représentant les caractéristiques d'un produit
    public productId: number;
    public name: string;
    public weight: number;
    public price: number;
    public dimensions: Dimensions;
    
    // Constructeur de la classe Product, initialise les propriétés du produit avec les valeurs passées en paramètre
    constructor(productId: number, name: string, weight: number, price: number, dimensions: Dimensions) { 
        this.productId = productId
        this.name = name
        this.weight = weight
        this.price = price 
        this.dimensions = dimensions
    }

    // Méthode pour afficher les details de la classe product
    displayDetails(): string { 

        return `Product ID:" ${this.productId}, "Name:" ${this.name}, "Weight:" ${this.weight},
         "Price:" ${this.price} "€", "Dimensions": ${this.dimensions.lenght}, ${this.dimensions.width}, ${this.dimensions.heigt}` 
    }
}
