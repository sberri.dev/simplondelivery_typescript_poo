import Product from "./product";
import { ShoeSize } from "../Types/shoesize";
import { Dimensions } from "../Types/dimensions";

//Création de la classe enfant Shoes avec la proprietés de pointure
class Shoes extends Product { 
    // Propriété de pointure de type ShoeSize
    public size: ShoeSize

    // Constructeur prenant les proprités de la classe parente ainsi que la nouvelle propriété enfant (pointure)
    constructor(productId: number, name: string, weight: number, price: number, dimensions: Dimensions, size: ShoeSize) {
        super(productId, name, weight, price, dimensions);
        this.size = size;
    }
    //Méthode pour affichés les informations de la classe parente et la classe enfant
    displayDetails(): string { 
        return super.displayDetails() + `"Shoes:" ${this.size}`
    }
}

export default Shoes; 