## Installation de Typescript

- Initialiser NPM 

```bash 
   npm init -y
```
- Ajouter la dépendance Typescript 

```bash 
   npm i --save-dev typescript @types/node
```
- Il faut créer 2 dossiers un qui se nomme "src" dans lequel on créera le fichier index.ts (pour le code Typescript) et l'autre "dist", dans lequel il se trouvera le fichier index.js (la transpilation en javascript)

- Création du fichier de configuration tsconfig.json avec toutes les fonctionnalités supportées par la version Node 18 et copiez/coller ce code ci-dessous

```json
   {
  "compilerOptions": {
    "module": "nodenext",
    "moduleResolution": "nodenext",
    "target": "es2022",
    "strict": true,
    "esModuleInterop": true,
    "forceConsistentCasingInFileNames": true,
    "outDir": "./dist"
  },

  "include": ["src/**/*"],
  "exclude": ["**/*.spec.ts"]
}
```
- Ajoutez dans la partie script dans le package.json le "build" 

```json 
   "scripts": {
  "build": "tsc",
}
```
- Tapez la commande suivante dans le terminal elle permettra la création du fichier index.js dans le fichier ./dist

```bash 
   npm run build
```
- Pour lancer l'application et exécuter le fichier index.js, taper la commande : 

```bash 
   node dist
```
